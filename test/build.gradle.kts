import casperix.localization_to_code.plugin.GenerateLocalizationCode

plugins {
	id("casperix.localization_to_code") version "1.0-SNAPSHOT"
}

tasks.register<GenerateLocalizationCode>("generateLocalizationCode") {
	inputDirectory.set(File("locale"))
	outputDirectory.set(File("generated"))
	className.set("casperix.generated.Messages")
}