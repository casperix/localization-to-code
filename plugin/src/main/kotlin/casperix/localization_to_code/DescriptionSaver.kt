package casperix.localization_to_code

import casperix.localization_to_code.item.ItemDescription
import java.io.File

object DescriptionSaver {

	fun write(outputFileDirectory: String, description: ItemDescription) {
		return writeFile(outputFileDirectory + File.separator + description.reference.packagePath, description.reference.name + ".kt", description.content)
	}

	private fun writeFile(filePath: String, fileName: String, outputText: String) {
		val dir = File(filePath)
		dir.mkdirs()

		val file = File(filePath + File.separator + fileName)
		file.writeText(outputText)
	}

}