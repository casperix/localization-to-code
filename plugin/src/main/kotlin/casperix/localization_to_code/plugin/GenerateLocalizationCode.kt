package casperix.localization_to_code.plugin

import casperix.localization_to_code.Executor
import org.gradle.api.DefaultTask
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Classpath
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

abstract class GenerateLocalizationCode : DefaultTask() {

	@get:InputDirectory
	abstract val inputDirectory: DirectoryProperty

	@get:OutputDirectory
	abstract val outputDirectory: DirectoryProperty

	@get:Classpath
	abstract val className: Property<String>

	@TaskAction
	fun generate() {
		val inputPath = inputDirectory.get().asFile.path
		val outputPath = outputDirectory.get().asFile.path
		val className = className.get()

		Executor.execute(inputPath, outputPath, className, project.displayName)
	}
}