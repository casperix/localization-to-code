package casperix.localization_to_code

import casperix.localization_to_code.factory.GeneratorConfig
import casperix.localization_to_code.factory.PropertiesDescriptionFactory
import java.io.File
import java.time.Instant
import java.util.*


object Executor {

	fun execute(inputFileDirectory: String, outputFileDirectory: String, fullClassName: String, customer:String) {
		val api = ItemReferenceParser.parse(fullClassName)
		val config = GeneratorConfig(customer, Date(), api)
		val inputFileList = File(inputFileDirectory).listFiles() ?: throw Error("Expected input directory $inputFileDirectory")
		val propertyTables = PropertyParser.parseInputFiles(inputFileList)
		val items = PropertiesDescriptionFactory(config).generate(propertyTables)

		items.forEach {
			DescriptionSaver.write(outputFileDirectory, it)
		}
	}
}