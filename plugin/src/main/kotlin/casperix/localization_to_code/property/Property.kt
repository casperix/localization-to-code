package casperix.localization_to_code.property

class Property(val content: String, val type: PropertyType)

interface PropertyType {
	val name:String
}

class PropertyTable(val fileKey: String, val properties: Map<String, Property>)

object StringPropertyType : PropertyType {
	override val name = "String"
}

class CustomPropertyType(val values: List<String>) : PropertyType {
	override val name = "Generated_" + values.joinToString("_")
}