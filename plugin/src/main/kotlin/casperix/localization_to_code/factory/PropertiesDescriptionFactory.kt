package casperix.localization_to_code.factory

import casperix.localization_to_code.item.ItemDescription
import casperix.localization_to_code.item.ItemReference
import casperix.localization_to_code.property.PropertyTable

class PropertiesDescriptionFactory(val config: GeneratorConfig) {
	val selector = config.api.copy(name = config.api.name + "Selector")

	fun generate(propertyTables: List<PropertyTable>): List<ItemDescription> {
		val properties = propertyTables.flatMap { table ->
			table.properties.map { (k, v) ->
				Pair(k, v)
			}
		}.toMap()

		val propertyTypes = properties.values.map {
			it.type
		}.toSet()
		val propertyTypeItems = PropertyTypeFactory.generateTypes(config, propertyTypes)
		val propertyTypeReferences = propertyTypeItems.map { it.reference }

		val fileKeys = propertyTables.map { table ->
			table.fileKey
		}.toSet()
		val selectorItem = PropertySelectorFactory.generate(config, fileKeys, selector)

		val apiItem = PropertyAPIFactory.generate(config, properties, propertyTypeReferences)
		val tablesItem = propertyTables.map { info ->
			val self = HelperFactory.createImplClass(config.api, info.fileKey)
			PropertyTableFactory.generate(config, info.properties, propertyTypeReferences, self, config.api)
		}

		return (tablesItem + apiItem + selectorItem + propertyTypeItems)
	}
}