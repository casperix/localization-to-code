package casperix.localization_to_code.factory

import casperix.localization_to_code.item.ItemReference
import java.util.*

class GeneratorConfig(val customer: String, val date: Date, val api: ItemReference)