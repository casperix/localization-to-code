package casperix.localization_to_code.factory

import casperix.localization_to_code.item.ItemDescription
import casperix.localization_to_code.item.ItemReference
import casperix.localization_to_code.property.CustomPropertyType
import casperix.localization_to_code.property.PropertyType

object PropertyTypeFactory {
	val propertyClassTemplate = """class %1(val template: String) {
	fun build(%2): String {
		return template
%3
	}
}"""

	fun generate(config: GeneratorConfig, values: List<String>, self: ItemReference): ItemDescription {
		val argumentList = values.map { "$it: String" }.joinToString(", ")
		val replaceList = values.map { "\t\t\t.replace(\"%$it\", $it)" }.joinToString("\n")

		val propertyContent = propertyClassTemplate
			.replace("%1", self.name)
			.replace("%2", argumentList)
			.replace("%3", replaceList)

		val content = AbstractFileFactory.buildPackage(self) +
				AbstractFileFactory.buildComments(config) +
				propertyContent

		return ItemDescription(self, content)
	}

	fun generateTypes(config: GeneratorConfig, propertyTypes: Set<PropertyType>): List<ItemDescription> {
		val baseClass = config.api
		return propertyTypes.mapNotNull { propertyType ->
			if (propertyType is CustomPropertyType) {
				val self = baseClass.copy(packs = baseClass.packs + "impl", propertyType.name)
				generate(config, propertyType.values, self)
			} else {
				null
			}
		}
	}
}