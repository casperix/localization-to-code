package casperix.localization_to_code.factory

import casperix.localization_to_code.item.ItemReference

object HelperFactory {

	fun createImplClass(baseClass: ItemReference, fileKey: String): ItemReference {
		return baseClass.copy(packs = baseClass.packs + "impl", name = baseClass.name + fileKey.capitalize())
	}
}

