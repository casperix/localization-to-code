package casperix.localization_to_code.factory

import casperix.localization_to_code.item.ItemDescription
import casperix.localization_to_code.item.ItemReference

object PropertySelectorFactory {
	private val enumLineTemplate = "	%1(%2),"

	fun generate(config: GeneratorConfig, keys: Set<String>, self: ItemReference): ItemDescription {
		val baseName = config.api
		val classes = keys.map {
			Pair(it, HelperFactory.createImplClass(baseName, it))
		}.toMap()

		val enumContent = classes.map { (key, clazz) ->
			enumLineTemplate.replace("%1", key).replace("%2", clazz.name)
		}.joinToString("\n")

		val content = AbstractFileFactory.buildPackage(baseName) +
				AbstractFileFactory.buildImports(classes.values) +
				AbstractFileFactory.buildComments(config) +
				AbstractFileFactory.buildEnum(self, baseName, enumContent)

		return ItemDescription(self, content)
	}
}