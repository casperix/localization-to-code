package casperix.localization_to_code.factory

import casperix.localization_to_code.item.ItemDescription
import casperix.localization_to_code.item.ItemReference
import casperix.localization_to_code.property.Property
import casperix.localization_to_code.property.StringPropertyType

object PropertyTableFactory {
	private val stringPattern = "	override val %1 = \"%3\""
	private val customPattern = "	override val %1 = %2(\"%3\")"

	fun generate(config: GeneratorConfig, lines: Map<String, Property>, propertyTypeReferences: List<ItemReference>, self: ItemReference, parent: ItemReference): ItemDescription {

		val classContent = lines.map { (key, property) ->
			val safeValue = property.content.replace("\"", "\\\"")
			val pattern = if (property.type is StringPropertyType) stringPattern else customPattern

			pattern
				.replace("%1", key)
				.replace("%2", property.type.name)
				.replace("%3", safeValue)

		}.joinToString("\n")

		val content = AbstractFileFactory.buildPackage(self) +
				AbstractFileFactory.buildImports(listOf(parent) + propertyTypeReferences) +
				AbstractFileFactory.buildComments(config) +
				AbstractFileFactory.buildObject(self, parent, classContent)

		return ItemDescription(self, content)
	}
}