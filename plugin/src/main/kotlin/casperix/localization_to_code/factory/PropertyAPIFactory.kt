package casperix.localization_to_code.factory

import casperix.localization_to_code.item.ItemDescription
import casperix.localization_to_code.item.ItemReference
import casperix.localization_to_code.property.Property

object PropertyAPIFactory {
	private val interfaceLineTemplate = "	val %1: %2"

	fun generate(config: GeneratorConfig, properties: Map<String, Property>, propertyTypeReferences:List<ItemReference>): ItemDescription {
		val self = config.api
		val interfaceContent = properties.map { (key, property) ->
			interfaceLineTemplate.replace("%1", key).replace("%2", property.type.name)
		}.joinToString("\n")

		val content = AbstractFileFactory.buildPackage(self) +
				AbstractFileFactory.buildImports(propertyTypeReferences) +
				AbstractFileFactory.buildComments(config) +
				AbstractFileFactory.buildInterface(self, interfaceContent)

		return ItemDescription(self, content)
	}
}