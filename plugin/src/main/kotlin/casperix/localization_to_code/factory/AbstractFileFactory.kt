package casperix.localization_to_code.factory

import casperix.localization_to_code.item.ItemReference
import java.text.DateFormat
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

/**
 *
 */
object AbstractFileFactory {
	private val packageTemplate = "package %1\n\n"
	private val importTemplate = "import %1\n"

	private val interfaceTemplate = """interface %1 {
%2
}
"""

	private val objectWithBaseTemplate = """object %1 : %2 {
%3
}
"""

	private val enumTemplate = """enum class %1(val source: %2) {
%3
}
"""

	fun buildObject(self: ItemReference, base: ItemReference, content: String): String {
		return objectWithBaseTemplate.replace("%1", self.name).replace("%2", base.name).replace("%3", content)
	}

	fun buildEnum(self: ItemReference, base: ItemReference, content: String): String {
		return enumTemplate.replace("%1", self.name).replace("%2", base.name).replace("%3", content)
	}

	fun buildInterface(self: ItemReference, content: String): String {
		return interfaceTemplate.replace("%1", self.name).replace("%2", content)
	}

	fun buildPackage(self: ItemReference): String {
		return packageTemplate.replace("%1", self.packageName)
	}

	fun buildImports(items: Collection<ItemReference>): String {
		return items.toSet().map { clazz ->
			importTemplate.replace("%1", clazz.fullClassName)
		}.joinToString("") + "\n"
	}

	fun buildComments(config: GeneratorConfig): String {
		val date = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.ENGLISH).format(config.date)
//		val date = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(ZoneId.of("UTC")).format(config.date)
		val projectUrl = "https://gitlab.com/casperix/localization-to-code"
		val mainMessage = "This file is generated automatically.".uppercase()

		return """/**
 *		$mainMessage
 * 
 * 	Customer: ${config.customer}
 * 	Date: $date
 *		See [localization-to-code]($projectUrl) 
 *
 */
 """
	}

}

