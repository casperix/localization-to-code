package casperix.localization_to_code.item

import java.io.File

data class ItemReference(val packs:List<String>, val name:String) {
	val packageName = 	packs.joinToString(".")
	val fullClassName = "$packageName.$name"

	val packagePath = 	packs.joinToString(File.separator)
}

