package casperix.localization_to_code.item

data class ItemDescription(val reference: ItemReference, val content:String)