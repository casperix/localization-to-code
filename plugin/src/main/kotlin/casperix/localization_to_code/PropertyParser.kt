package casperix.localization_to_code

import casperix.localization_to_code.property.CustomPropertyType
import casperix.localization_to_code.property.Property
import casperix.localization_to_code.property.PropertyTable
import casperix.localization_to_code.property.StringPropertyType
import java.io.File

object PropertyParser {
	private val propertyTemplate = Regex("([^:=\\s]*)\\s*[:=]\\s*(.*)")
	private val variableTemplate = Regex("%([^\\d\\W]+\\w*)")

	fun parseInputFiles(inputFileList: Array<out File>): List<PropertyTable> {
		return inputFileList.map { inputFile ->
			val fileKey = inputFile.nameWithoutExtension
			val inputText = inputFile.readText()
			val map = parseTable(inputText).map { (k, v) -> Pair(k, parseProperty(v)) }.toMap()
			PropertyTable(fileKey, map)
		}
	}

	fun parseProperty(content: String): Property {
		val result = variableTemplate.findAll(content).toList()
		if (result.isEmpty()) {
			return Property(content, StringPropertyType)
		}
		val arguments = result.map {
			it.groupValues.last()
		}.toSet()
		return Property(content, CustomPropertyType(arguments.sorted()))
	}


	private fun parseTable(text: String): Map<String, String> {
		val lines = text.split("\n")
		return lines.mapNotNull {
			parseLine(it)
		}.toMap()
	}

	fun parseLine(it: String): Pair<String, String>? {
		val result = propertyTemplate.find(it)
			?: return null

		val nameValues = result.groupValues
		return if (nameValues.size == 3) {
			val name = nameValues[1]
			val value = nameValues[2]
			Pair(name, value)
		} else {
			null
		}
	}
}