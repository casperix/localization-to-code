package casperix.localization_to_code

import casperix.localization_to_code.item.ItemReference

object ItemReferenceParser {
	fun parse(fullClassName: String): ItemReference {
		val parts = fullClassName.split(".").toMutableList()
		val className = parts.removeLast()
		return ItemReference(parts, className)
	}
}