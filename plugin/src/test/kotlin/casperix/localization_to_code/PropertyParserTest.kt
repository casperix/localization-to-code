package casperix.localization_to_code

import kotlin.test.Test
import kotlin.test.assertEquals


class PropertyParserTest {

	@Test
	fun parseLine() {
		assertEquals(Pair("a", "b"), PropertyParser.parseLine("a=b"))
		assertEquals(Pair("a", "b"), PropertyParser.parseLine("a:b"))
		assertEquals(Pair("a", "b"), PropertyParser.parseLine("a :b"))
		assertEquals(Pair("a", "b"), PropertyParser.parseLine("a: b"))
		assertEquals(Pair("a", "b"), PropertyParser.parseLine("a : b"))
		assertEquals(Pair("a", "b"), PropertyParser.parseLine("a\t:\tb"))

		val rus = "перевод на другой язык"
		val eng = "translation into another language"
		val chi = "翻译成另一种语言"
		val arb = "الترجمة إلى لغة أخرى"

		assertEquals(Pair("a", eng), PropertyParser.parseLine("a : $eng"))
		assertEquals(Pair("a", rus), PropertyParser.parseLine("a : $rus"))
		assertEquals(Pair("a", chi), PropertyParser.parseLine("a : $chi"))
		assertEquals(Pair("a", arb), PropertyParser.parseLine("a : $arb"))
	}

	@Test
	fun parseProperty() {
		val property = PropertyParser.parseProperty("some %1 %333 %% % as% %s %ss %s1 %s11 %s1s")

		val type = property.type
		if (type is CustomPropertyType) {
			assertEquals(type.values, setOf("s", "ss", "s1", "s11", "s1s").sorted())
		} else {
			assert(false) { "Invalid property type" }
		}

	}
}